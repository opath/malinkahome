#!/usr/bin/pythonRoot
from flup.server.fcgi import WSGIServer 
import urlparse
import smbus
import json

from functools import partial
from timerUpDown import TimerUpDown
from relay import Relay
from realButton import RealButton
from blindObserver import Observer
from blindFsmFactory import BlindFactory

bus = smbus.SMBus(1)  # New Rev 2 board 
smbusAddressLivingRoom = 0x20
smbusAddressBedRoom = 0x23

bus.write_byte_data(smbusAddressLivingRoom, 0x00, 0x00)
bus.write_byte_data(smbusAddressBedRoom, 0x00, 0x00)

timerLivingRoomWindow = TimerUpDown(24, 22)
timerLivingRoomTerrace = TimerUpDown(31, 27.5)
timerBedRoom = TimerUpDown(23, 21)

blindObserver = Observer()
livingRoomWindowObserver = partial(blindObserver.update, 'LivingRoomWindow')
livingRoomTerraceObserver = partial(blindObserver.update, 'LivingRoomTerrace')
bedRoomObserver = partial(blindObserver.update, 'BedRoom')

relayUpLivingRoomWindow = Relay(bus, smbusAddressLivingRoom, 0x40)
relayDownLivingRoomWindow = Relay(bus, smbusAddressLivingRoom, 0x80)
blindLivingRoomWindow = BlindFactory().createBlind(relayUpLivingRoomWindow, relayDownLivingRoomWindow, timerLivingRoomWindow, livingRoomWindowObserver)

relayUpLivingRoomTerrace = Relay(bus, smbusAddressLivingRoom, 0x20)
relayDownLivingRoomTerrace = Relay(bus, smbusAddressLivingRoom, 0x10)
blindLivingRoomTerrace = BlindFactory().createBlind(relayUpLivingRoomTerrace, relayDownLivingRoomTerrace, timerLivingRoomTerrace, livingRoomTerraceObserver)

relayUpBedRoom = Relay(bus, smbusAddressBedRoom, 0x40)
relayDownBedRoom = Relay(bus, smbusAddressBedRoom, 0x80)
blindBedRoom = BlindFactory().createBlind(relayUpBedRoom, relayDownBedRoom, timerBedRoom, bedRoomObserver)

def app(environ, start_response):
    start_response('200 OK', [('Content-Type', 'application/json')]) 
    i = urlparse.parse_qs(environ["QUERY_STRING"])
  
    if "q" in i:
        if i["q"][0] == "LivingRoomWindowOpen": 
            blindLivingRoomWindow.open()
        elif i["q"][0] == "LivingRoomWindowClose":
            blindLivingRoomWindow.close()
        elif i["q"][0] == "LivingRoomTerraceOpen":
            blindLivingRoomTerrace.open()
        elif i["q"][0] == "LivingRoomTerraceClose":
            blindLivingRoomTerrace.close()
        elif i["q"][0] == "BedRoomOpen":
            blindBedRoom.open()
        elif i["q"][0] == "BedRoomClose":
            blindBedRoom.close()
        elif i["q"][0] == "status":
            yield json.dumps(blindObserver.getStatus())


#buttons
livingRoomWindowButton = RealButton(bus, 0x38, blindLivingRoomWindow, 253, 254)
livingRoomWindowButton.start()
bedRoomButton = RealButton(bus, 0x38, blindBedRoom, 251, 247)
bedRoomButton.start()
livingRoomTerraceButton = RealButton(bus, 0x38, blindLivingRoomTerrace, 239, 223)
livingRoomTerraceButton.start()


WSGIServer(app).run()
livingRoomWindowButton.stop()
livingRoomTerraceButton.stop()
bedRoomButton.stop()
