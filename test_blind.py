from blindFSM import BlindFSM
from mock import Mock 
import pytest
from blindFsmFactory import BlindFsmFactory

relayUp = Mock()
relayUp.testMethod("on")
relayUp.testMethod("off")
relayDown = Mock()
relayDown.testMethod("on")
relayDown.testMethod("off")
timer = Mock()
timer.testMethod("stop")
timer.testMethod("setTimeoutUp")
timer.testMethod("setTimeoutDown")
observer = Mock()
blindFactory = BlindFsmFactory()

@pytest.fixture()
def before():
    relayUp.reset_mock()
    relayDown.reset_mock()
    timer.reset_mock()

def test_blindShallNotOpenIfAlreadyOpen(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "open"
    blind.open()
    assert not relayUp.on.called

def test_blindShallClose(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.close()
    assert relayDown.on.called
    assert timer.setTimeoutDown.called
    assert blind.FSM.curStateName == "closing"

def test_blindShallNotCloseIfAlreadyClosed(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "close"
    blind.close()
    assert not relayDown.on.called

def test_blindShallBeClosedAfterTimeout():
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "open"
    blind.close()
    blind.timeOut()

    assert blind.FSM.curStateName == "close"
    assert relayDown.off.called

def test_blindShallOpenIfClosed(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "close"
    blind.open()

    assert relayUp.on.called
    assert timer.setTimeoutUp.called

def test_blindShallBeOpenAfterTimeout(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "close"
    blind.open()
    blind.timeOut()

    assert blind.FSM.curStateName == "open"
    assert relayUp.off.called

def test_blidShallStopIfCloseHitDuringClosing(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "closing"
    blind.close()

    assert relayDown.off.called
    assert blind.FSM.curStateName == "stopped"
    assert timer.stop.called

def test_blidShallStopIfOpenHitDuringClosing(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "closing"
    blind.open()

    assert relayDown.off.called
    assert blind.FSM.curStateName == "stopped"
    assert timer.stop.called

def test_blindShallContinueClosingIfStopped(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.close()
    blind.close() #stop closing
    relayDown.reset_mock()
    timer.reset_mock()
    blind.close()

    assert relayDown.on.called
    assert blind.FSM.curStateName == "closing"
    assert timer.setTimeoutDown.called

def test_blindShallOpenIfCloseWasStopedBefore():
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.close()
    blind.close() #stop closing
    timer.reset_mock()

    blind.open()

    assert relayUp.on.called
    assert blind.FSM.curStateName == "opening"
    assert timer.setTimeoutUp.called   

def test_blidShallStopIfCloseHitDuringOpening(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "close"
    blind.open()
    blind.close() # stop opening

    assert relayUp.off.called
    assert blind.FSM.curStateName == "stopped"
    assert timer.stop.called

def test_blidShallStopIfOpenHitDuringOpening(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "close"
    blind.open()
    blind.open() # stop opening

    assert relayUp.off.called
    assert blind.FSM.curStateName == "stopped"
    assert timer.stop.called


def test_blindShallCloseIfOpenWasStopedBefore():
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)
   
    blind.FSM.curStateName = "close"
    blind.open()
    blind.open() #stop openingi
    timer.reset_mock()

    blind.close()

    assert relayDown.on.called
    assert blind.FSM.curStateName == "closing"
    assert timer.setTimeoutDown.called

def test_blindShallContinueOpeningIfStopped(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "close"
    blind.open()
    blind.open() #stop opening 
    relayUp.reset_mock()
    timer.reset_mock()
    blind.open()

    assert relayUp.on.called
    assert blind.FSM.curStateName == "opening"
    assert timer.setTimeoutUp.called

def test_openCloseOpen(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "close"
    blind.open()
    blind.close() #stop opening 
    relayUp.reset_mock()
    timer.reset_mock()
    blind.open()

    assert relayUp.on.called
    assert blind.FSM.curStateName == "opening"
    assert timer.setTimeoutUp.called

def test_openCloseClose(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.FSM.curStateName = "close"
    blind.open()
    blind.close() #stop opening 
    timer.reset_mock()
    blind.close()

    assert relayDown.on.called
    assert blind.FSM.curStateName == "closing"
    assert timer.setTimeoutDown.called

def test_closeOpenClose(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.close()
    blind.open() #stop opening 
    timer.reset_mock()
    blind.close()

    assert relayDown.on.called
    assert blind.FSM.curStateName == "closing"
    assert timer.setTimeoutDown.called

def test_closeOpenOpen(before):
    blind = blindFactory.createBlind(relayUp, relayDown, timer, observer)

    blind.close()
    blind.open() #stop opening 
    timer.reset_mock()
    blind.open()

    assert relayDown.on.called
    assert blind.FSM.curStateName == "opening"
    assert timer.setTimeoutUp.called

