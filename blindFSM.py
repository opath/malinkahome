class BlindFSM():
    def __init__(self, fsm, observer):
        self.observer = observer
        self.FSM = fsm 
        self.observer(state = self.FSM.curStateName)

    def open(self):
        if self.FSM.curStateName == "opening" or self.FSM.curStateName == "closing":
            self.FSM.transition("toStopped")
        elif self.FSM.curStateName == "close" or self.FSM.curStateName == "stopped":
            self.FSM.transition("toOpening")
        self.FSM.execute()    
        self.observer(state = self.FSM.curStateName)
              
    def close(self):
        if self.FSM.curStateName == "opening" or self.FSM.curStateName == "closing":
            self.FSM.transition("toStopped")
        elif self.FSM.curStateName == "open" or self.FSM.curStateName == "stopped":
            self.FSM.transition("toClosing")
        self.FSM.execute()    
        self.observer(state = self.FSM.curStateName)

    def timeOut(self):
        if self.FSM.curStateName == "opening":
            self.FSM.transition("toOpen")
        else:
            self.FSM.transition("toClose")
        self.FSM.execute()    
        self.observer(state = self.FSM.curStateName)
