import threading
import time

class TimerUpDown():
    timer = 0
    timestamp = 0
 
    def __init__(self, periodUp, periodDown):
        self.periodUp = periodUp
        self.periodDown = periodDown
 
   
    def setTimeoutUp(self):
        self.__setTimeout(self.periodUp)

    def setTimeoutDown(self):
        self.__setTimeout(self.periodDown)  
        
    def setCallBackFun(self, callback):
        self.callback = callback

    def stop(self):
        if self.timer != 0:
            self.timer.cancel()
            self.timer = 0
            self.timestamp

    def getTimeToTimeOut(self):
        return self.period - (time.time() - self.timestamp)

    def __setTimeout(self, period):
        self.timer = threading.Timer(period, self.callback)
        self.timer.start()
        self.timestamp = time.time()
        self.period = period
