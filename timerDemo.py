from timer import Timer
from timerUpDown import TimerUpDown
import time

timer = TimerUpDown(4, 5)

def callback():
    print timer.getTimeToTimeOut()

timer.setCallBackFun(callback)
timer.setTimeoutUp()

time.sleep(3)

print timer.getTimeToTimeOut()
