class Observer():
    def __init__(self):
        self.relays = {}      
    
    def update(self, args, **kwargs):
        self.relays[args] = kwargs
    
    def getStatus(self):
        return self.relays
    
#observerDemo = Observer()
#observerDemo.update('relay1', status = 'open')
