import time
import threading

class RealButton(threading.Thread):
    def __init__(self, bus, addr, blind, upCode, downCode):
        threading.Thread.__init__(self)
        self.bus = bus 
        self.addr = addr
        self.blind = blind
        self.upCode = upCode
        self.downCode = downCode
        self.isRuning = False

        self.buttonDelayTime = 0.1
        self.noSignalOnPin = 255
        self.aferPushDelayTime = 0.6

    def run(self):
        self.isRuning = True
        while self.isRuning:
            time.sleep(self.buttonDelayTime)
            pins = self.bus.read_byte(self.addr)
            if pins != self.noSignalOnPin:
                if pins == self.upCode:
                    self.blind.open()
                elif pins == self.downCode:
                    self.blind.close()
                time.sleep(self.aferPushDelayTime)

    def stop(self):
        self.isRuning = False
