State = type("State", (object,), {})

class Transition(object):
    def __init__(self, toState):
        self.toState = toState

    def execute(self):
        pass

class StateMachine(object):
    def __init__(self):
        self.states = {}
        self.transitions = {}
        self.curState = None
        self.curStateName = None
        self.trans = None
    
    def addState(self, stateName, state):
        self.states[stateName] = state

    def addTransition(self, transName, transition):
        self.transitions[transName] = transition
        
    def setState(self, stateName):
        self.curState = self.states[stateName]
        self.curStateName = stateName

    def transition(self, transName):
        self.trans = self.transitions[transName]
        
    def execute(self):
        if (self.trans):
            self.trans.execute()
            self.setState(self.trans.toState)
            self.trans = None
            self.curState.execute()
