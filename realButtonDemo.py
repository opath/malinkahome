#!/usr/bin/pythonRoot
import smbus

from blind import Blind
from timer import Timer
from relay import Relay
from realButton import RealButton

bus = smbus.SMBus(1)  # New Rev 2 board 
smbusAddressLivingRoom = 0x20
smbusAddressBedRoom = 0x23

bus.write_byte_data(smbusAddressLivingRoom, 0x00, 0x00)
bus.write_byte_data(smbusAddressBedRoom, 0x00, 0x00)

timerLivingRoomWindow = Timer()
timerLivingRoomTerrace = Timer()
timerBedRoom = Timer()

relayUpLivingRoomWindow = Relay(bus, smbusAddressLivingRoom, 0x40)
relayDownLivingRoomWindow = Relay(bus, smbusAddressLivingRoom, 0x80)
blindLivingRoomWindow = Blind(relayUpLivingRoomWindow, relayDownLivingRoomWindow, timerLivingRoomWindow, 24, 22)

relayUpLivingRoomTerrace = Relay(bus, smbusAddressLivingRoom, 0x20)
relayDownLivingRoomTerrace = Relay(bus, smbusAddressLivingRoom, 0x10)
blindLivingRoomTerrace = Blind(relayUpLivingRoomTerrace, relayDownLivingRoomTerrace, timerLivingRoomTerrace, 31, 27.5)

relayUpBedRoom = Relay(bus, smbusAddressBedRoom, 0x40)
relayDownBedRoom = Relay(bus, smbusAddressBedRoom, 0x80)
blindBedRoom = Blind(relayUpBedRoom, relayDownBedRoom, timerBedRoom, 23, 21)

bus.write_byte(0x38, 0xff)
livingRoomWindowButton = RealButton(bus, 0x38, blindLivingRoomWindow, 254, 253)
livingRoomWindowButton.start()

bedRoomButton = RealButton(bus, 0x38, blindBedRoom, 251, 247)
bedRoomButton.start()


