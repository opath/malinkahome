from stateMachine import StateMachine
from stateMachine import State
from stateMachine import Transition
from blindFSM import BlindFSM

class BlindOpen(State):
    def __init__(self, relayUp):
        self.relayUp = relayUp
    def execute(self): 
        self.relayUp.off()

class BlindClose(State):
    def __init__(self, relayDown):
        self.relayDown = relayDown
    def execute(self): 
        self.relayDown.off()

class BlindOpening(State):
    def __init__(self, relayUp, timer):
        self.relayUp = relayUp
        self.timer = timer
    def execute(self): 
        self.relayUp.on()
        self.timer.setTimeoutUp()

class BlindStopped(State):
    def __init__(self, relayUp, relayDown, timer):
        self.relayUp = relayUp
        self.relayDown = relayDown
        self.timer = timer
    def execute(self):
        self.timer.stop()
        self.relayUp.off()
        self.relayDown.off()

class BlindCloseing(State):
    def __init__(self, relayDown, timer):
        self.relayDown = relayDown
        self.timer = timer
    def execute(self):
        self.relayDown.on()
        self.timer.setTimeoutDown()

class BlindFsmFactory():
    def createFsm(self, relayUp, relayDown, timer):
        FSM = StateMachine()
        FSM.addState("open", BlindOpen(relayUp))
        FSM.addState("close", BlindClose(relayDown))
        FSM.addState("opening", BlindOpening(relayUp, timer))
        FSM.addState("closing", BlindCloseing(relayDown, timer))
        FSM.addState("stopped", BlindStopped(relayUp, relayDown, timer))

        FSM.addTransition("toOpen", Transition("open"))
        FSM.addTransition("toClose", Transition("close"))
        FSM.addTransition("toOpening", Transition("opening"))
        FSM.addTransition("toClosing", Transition("closing"))
        FSM.addTransition("toStopped", Transition("stopped"))

        FSM.curStateName = "open"

        return FSM 

class BlindFactory():
    def createBlind(self, relayUp, relayDown, timer, observer):

        FSM = BlindFsmFactory().createFsm(relayUp, relayDown, timer)
        blind = BlindFSM(FSM, observer)
        timer.setCallBackFun(blind.timeOut)
        
        return blind
