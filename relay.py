import RPi.GPIO as GPIO
import smbus
from time import sleep

class Relay:
    def __init__(self, bus, busAddress, relayAddressOnBus):
        self.busAddress = busAddress 
        self.relayAddressOnBus = relayAddressOnBus
        self.isOn = True 
        self.bus = bus
        self.off()

    def on(self):
        if self.isOn == False:
            relayStatus = self.bus.read_byte_data(self.busAddress, 0x09)
            bitToOn = relayStatus | self.relayAddressOnBus
            self.bus.write_byte_data(self.busAddress, 0x09, bitToOn)
            self.isOn = True

    def off(self):
        if self.isOn == True:
            relayStatus = self.bus.read_byte_data(self.busAddress, 0x09)
            bitToClear = relayStatus & ~self.relayAddressOnBus
            self.bus.write_byte_data(self.busAddress, 0x09, bitToClear)
            self.isOn = False
